require 'rails_helper'

RSpec.describe "Todos", type: :request do
  let!(:todos) { create_list(:todo, 10) }
  let(:todo_id) { todos.first.id }

  describe 'GET /todos' do
    context 'when the record exists' do
      it 'returns success' do
        get '/todos'
        expect(response.status).to eq 200
        expect(json).to be_present
        expect(json['todos'].size).to eq 10
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the index error' do
      it 'returns index error' do
        allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
        get '/todos'
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 3
        expect(json['error_message']).to eq '一覧の取得に失敗しました'
      end
    end

    context 'when the other error' do
      it 'returns internal server error' do
        allow(Todo).to receive(:select).and_raise(Exception)
        get '/todos'
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'POST /todos' do
    context 'when the record exists' do
      it 'returns success' do
        post '/todos', params: { 'title': 'test', 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the title is exactly' do
      it 'returns success' do
        post '/todos', params: { 'title': 't' * 100, 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the detail is exactly' do
      it 'returns success' do
        post '/todos', params: { 'title': 'test', 'detail': 'd' * 1000, 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the create error' do
      it 'returns create error' do
        allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
        post '/todos', params: { 'title': 'test', 'detail': 'detail', 'date': '2020-06-05' }
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 4
        expect(json['error_message']).to eq '登録に失敗しました'
      end
    end

    context 'when the request error' do
      it 'returns request error' do
        post '/todos', params: { 'title': '', 'detail': 'd' * 1000, 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the title is 101 characters' do
      it 'returns request error' do
        post '/todos', params: { 'title': 't' * 101, 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the detail is 1001 characters' do
      it 'returns request error' do
        post '/todos', params: { 'title': 'test', 'detail': 'd' * 1001, 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the other error' do
      it 'returns internal server error' do
        allow(Todo).to receive(:create!).and_raise(Exception)
        post '/todos', params: { 'title': 'test', 'detail': 'detail', 'date': '2020-06-05' }
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'PUT /todos/:id' do
    context 'when the record exists' do
      it 'returns success' do
        put "/todos/#{todo_id}", params: { 'title': 'test', 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the title is exactly' do
      it 'returns success' do
        put "/todos/#{todo_id}", params: { 'title': 't' * 100, 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the detail is exactly' do
      it 'returns success' do
        put "/todos/#{todo_id}", params: { 'title': 'test', 'detail': 'd' * 1000, 'date': '2020-06-05' }
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the update error' do
      it 'returns update error' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
        put "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
      end
    end

    context 'when the request error' do
      it 'returns request error' do
        put "/todos/#{todo_id}", params: { 'title': '', 'detail': 'd' * 1000, 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the title is 101 characters' do
      it 'returns request error' do
        put "/todos/#{todo_id}", params: { 'title': 't' * 101, 'detail': 'test', 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the detail is 1001 characters' do
      it 'returns request error' do
        put "/todos/#{todo_id}", params: { 'title': 'test', 'detail': 'd' * 1001, 'date': '2020-06-05' }
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the other error' do
      it 'returns internal server error' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'DELETE /todos/:id' do
    context 'when the record exists' do
      it 'returns success' do
        put "/todos/#{todo_id}"
        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'when the delete failed' do
      it 'returns delete failed' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
      end
    end

    context 'when the request error' do
      before { Todo.find(todo_id).destroy }

      it 'returns request error' do
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'when the other errors' do
      it 'returns internal server error' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"
        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end
end
