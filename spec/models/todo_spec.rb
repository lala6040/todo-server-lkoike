require 'rails_helper'

RSpec.describe Todo, type: :model do
  describe "validations" do
    describe "title" do
      it { is_expected.to validate_presence_of :title }
      it { should validate_length_of(:title).is_at_most(Todo::MAX_TITLE_LIMIT) }
    end

    describe "detail" do
      it { should validate_length_of(:detail).is_at_most(Todo::MAX_DETAIL_LIMIT) }
    end
  end
end
