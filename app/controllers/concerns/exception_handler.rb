module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from Exception, with: :render_internal_server_error
    rescue_from ActiveRecord::RecordInvalid, with: :render_bad_request
    rescue_from ActiveRecord::RecordNotFound, with: :render_bad_request
  end

  INTERNAL_SERVER_ERROR_CODE = 1
  BAD_REQUEST_ERROR_CODE = 2

  def render_internal_server_error
    render status: 500, json: { error_code: INTERNAL_SERVER_ERROR_CODE, error_message: 'サーバー内で不明なエラーが発生しました' }
  end

  def render_bad_request
    render status: 400, json: { error_code: BAD_REQUEST_ERROR_CODE, error_message: 'リクエストの形式が不正です' }
  end
end