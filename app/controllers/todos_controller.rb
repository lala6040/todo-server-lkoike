class TodosController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_todo, only: [:update, :destroy]

  NO_ERROR_CODE = 0
  INDEX_ERROR_CODE = 3
  CREATE_ERROR_CODE = 4
  UPDATE_ERROR_CODE = 5
  DESTROY_ERROR_CODE = 6

  def index
    todo_list = Todo.select(:id, :title, :detail, :date).order(:date)
    render status: 200, json: { todos: todo_list, error_code: NO_ERROR_CODE, error_message: '' }
  rescue ActiveRecord::ActiveRecordError
    render status: 500, json: { error_code: INDEX_ERROR_CODE, error_message: '一覧の取得に失敗しました' }
  end

  def create
    Todo.create!(todo_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render status: 500, json: { error_code: CREATE_ERROR_CODE, error_message: '登録に失敗しました' }
  end

  def update
    @todo.update!(todo_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render status: 500, json: { error_code: UPDATE_ERROR_CODE, error_message: '更新に失敗しました' }
  end

  def destroy
    @todo.destroy!
    render_success
  rescue ActiveRecord::ActiveRecordError
    render status: 500, json: { error_code: DESTROY_ERROR_CODE, error_message: '削除に失敗しました' }
  end

  private

  def todo_params
    params.permit(:title, :detail, :date)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end

  def render_success
    render status: 200, json: { error_code: NO_ERROR_CODE, error_message: '' }
  end
end
